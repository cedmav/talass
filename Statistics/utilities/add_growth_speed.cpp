#include <cstdio>
#include <cstdlib>
#include <vector>
#include <iostream>
#include <cstring>
#include <cmath>
#include <stack>

#include "Value.h"
#include "ClanHandle.h"
#include "AggregatorFactory.h"
#include "Attribute.h"
#include "FeatureHierarchy.h"
#include "FeatureSegmentation.h"

using namespace std;
using namespace TopologyFileFormat;
using namespace Statistics;


void maximum(const Feature* f, VertexCountArray& attr)
{

  // While we have not reached a root
  while ((f->repSize() > 0) && (attr[f->id()].value() > attr[f->rep(0)->id()].value())) {

    attr[f->rep(0)->id()] = attr[f->id()];
    f = f->rep(0);
  }
}


/*!
 *
 */

int main(int argc, const char* argv[])
{
  if (argc < 2) {
    fprintf(stderr,"Assuming the family file has a vertexCount field this will add an\
        unaggregated version call growth and the accumulated max_growth to the statistics\n");
    fprintf(stderr,"Usage: %s <family> \n",argv[0]);
    exit(0);
  }


  ClanHandle fclan,sclan;
  FamilyHandle family;
  FeatureHierarchy hierarchy;



  fclan.attach(argv[1]);
  family = fclan.family(0);
  hierarchy.initialize(family.simplification(0));

  hierarchy.parameter(1.0);
  //exit(0);




  VertexCountArray vertex_count(hierarchy.featureCount());
  VertexCountArray growth(hierarchy.featureCount());
  VertexCountArray max_growth(hierarchy.featureCount());

  // Get all aggregates
  const std::vector<StatHandle>& aggregates = family.aggregateList();

  auto it = aggregates.begin();

  while (it!=aggregates.end()) {
    if (it->stat() == "vertexCount")
      break;

    it++;
  }

  if (it == aggregates.end()) {
    fprintf(stderr,"ERROR: No aggregated vertexCount found\n");
    exit(0);
  }

  it->readData(&vertex_count);

  for (auto it=hierarchy.allFeatures().begin();it!=hierarchy.allFeatures().end();it++) {

    uint64_t incoming = 0;
    for (auto cIt = it->constituents().begin();cIt!=it->constituents().end();cIt++) {
      incoming += vertex_count[(*cIt)->id()].vertexCount();
    }

    //fprintf(stderr,"Found counts %d\n",vertex_count[it->id()].vertexCount());
    growth[it->id()].vertexCount(vertex_count[it->id()].vertexCount() - incoming);
    max_growth[it->id()].vertexCount(growth[it->id()].vertexCount());
  }

  fprintf(stderr,"Done computing ... starting accumulating\n");



  for (auto it=hierarchy.allFeatures().begin();it!=hierarchy.allFeatures().end();it++) {

    maximum(&(*it),max_growth);
  }


  fprintf(stderr,"Done accumulating... starting output\n");



  StatHandle derived_handle;

  derived_handle.aggregated(true);
  derived_handle.stat("Value");
  derived_handle.species("Growth");
  derived_handle.encoding(true);
  derived_handle.setData(&growth);

  fclan.family(0).append(derived_handle);

  derived_handle.species("Max_Growth");
  derived_handle.setData(&max_growth);

  fclan.family(0).append(derived_handle);

}


